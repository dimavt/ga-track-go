module github.com/dimavt/ga-track-go

require (
	github.com/gin-contrib/sse v0.0.0-20170109093832-22d885f9ecc7
	github.com/gin-gonic/gin v0.0.0-20170702092826-d459835d2b07
	github.com/golang/protobuf v0.0.0-20170601230230-5a0f697c9ed9
	github.com/mattn/go-isatty v0.0.0-20170307163044-57fdcb988a5c
	github.com/satori/go.uuid v1.2.0
	github.com/ugorji/go v0.0.0-20170215201144-c88ee250d022
	golang.org/x/sys v0.0.0-20180322165403-91ee8cde4354
	gopkg.in/go-playground/validator.v8 v8.18.1
	gopkg.in/yaml.v2 v2.0.0-20160928153709-a5b47d31c556
)
