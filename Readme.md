# Sitecheckup api

Трекинг событий из писем в Google Analytics

# Запуск
```
#собираем из исходников
docker build -t registry.gitlab.com/dimavt/ga-track-go .
# или сразу запускаем собранный из мастер ветки image
docker run --rm -p='8080:8080' registry.gitlab.com/dimavt/ga-track-go

```


# Использование
https://host/track?category="example_cat"&action="example_act"&label="example_label"&url="https://google.com"

# Настройка
Настраивается посредством двух env переменных:
- `GA_TRACKING_ID` - код аккаунта аналитики
- `SITE_URL` - Адресс куда делать редирект по умолчанию
