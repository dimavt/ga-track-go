# build stage
FROM golang:alpine AS build-env
ADD . /go/src/app
RUN cd /go/src/app && go build -o goapp

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /go/src/app /app/
ENTRYPOINT ./goapp
