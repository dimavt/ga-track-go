package main // import "github.com/dimavt/ga-track-go"

import (
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"github.com/satori/go.uuid"
)

import "github.com/gin-gonic/gin"

var gaPropertyID = mustGetenv("GA_TRACKING_ID")
var siteURL = mustGetenv("SITE_URL")

func mustGetenv(k string) string {
	v := os.Getenv(k)
	if v == "" {
		log.Fatalf("%s environment variable not set.", k)
	}
	return v
}

func main() {
	r := gin.Default()

	r.GET("/track", func(c *gin.Context) {
		// create copy to be used inside the goroutine
		cCp := c.Copy()
		urlToRedirect := c.Query("url")

		go func() {
			err := trackEvent(cCp.Request,
				cCp.Query("category"),
				cCp.Query("action"),
				cCp.Query("label"),
				nil)
			if err != nil {
				fmt.Printf("Event did not track: %v\n", err)
				return
			}
			fmt.Println("Event tracked.")
		}()

		if urlToRedirect == "" {
			urlToRedirect = siteURL
		}
		c.Redirect(302, urlToRedirect)
	})

	// Listen and serve on 0.0.0.0:8080
	r.Run(":8080")
}

func trackEvent(r *http.Request, category, action, label string, value *uint) error {
	if gaPropertyID == "" {
		return errors.New("analytics: GA_TRACKING_ID environment variable is missing")
	}
	if category == "" || action == "" {
		return errors.New("analytics: category and action are required")
	}

	v := url.Values{
		"v":   {"1"},
		"tid": {gaPropertyID},
		// Anonymously identifies a particular user. See the parameter guide for
		// details:
		// https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#cid
		//
		// Depending on your application, this might want to be associated with the
		// user in a cookie.
		"cid": {uuid.NewV4().String()},
		"t":   {"event"},
		"ec":  {category},
		"ea":  {action},
		"ua":  {r.UserAgent()},
	}

	if label != "" {
		v.Set("el", label)
	}

	if value != nil {
		v.Set("ev", fmt.Sprintf("%d", *value))
	}

	if remoteIP, _, err := net.SplitHostPort(r.RemoteAddr); err != nil {
		v.Set("uip", remoteIP)
	}

	// NOTE: Google Analytics returns a 200, even if the request is malformed.
	_, err := http.PostForm("https://www.google-analytics.com/collect", v)
	return err
}
